﻿/*  Script for testing out SQLite in Javascript
          2011 - Alan Chatham
          Released into the public domain
 
        This script is a GUI script - attach it to your main camera.
        It creates/opens a SQLite database, and with the GUI you can read and write to it.
                                        */
 
// This is the file path of the database file we want to use
// Right now, it'll load TestDB.sqdb in the project's root folder.
// If one doesn't exist, it will be automatically created.
public var DatabaseName : String = "veekun-pokedex.sqlite";
 
// This is the name of the table we want to use
public var TableName : String = "pokemon_species_names";
var db : dbAccess;
var pkdfont : Font;
var style : GUIStyle;
var sty:GUIStyle;
var images : Texture2D[];

function Start(){
    // Give ourselves a dbAccess object to work with, and open it
    db = new dbAccess();
    db.OpenDB(DatabaseName);
    style.normal.textColor = Color.black;
    sty.font = pkdfont;
    sty.normal.textColor = Color.black;
    // Let's make sure we've got a table to work with as well!
    var tableName = TableName;
    var columnNames = new Array("pokemon_species_id","name");
    
    
}
private var imagess = [];
function Awake(){
	imagess = LoadSequence("Images/Pokedex/Sprites/",150);

}
 
// These variables just hold info to display in our GUI
var num ="";
var DatabaseEntryStringWidth = 110;
var scrollPosition : Vector2;
var databaseData : ArrayList = new ArrayList();
var pkd : Texture;
var guii : GUITexture;
var call : AudioClip[];
var cube : GameObject;
 
// This GUI provides us with a way to enter data into our database
//  as well as a way to view it
public static function LoadSequence(namePrefix : String, length : int) : Array{

 

    var bars = Array(length);

    var i=0;

    for (i=0;i<length;i++)

    {

        var j = i + 1;

        if (j < 10){

            Debug.Log("loading " + namePrefix +  j);

            bars[i] = Resources.Load(namePrefix +  j, Texture);

        }

        else if (j < 154){

            Debug.Log("loading " + namePrefix + j);

            bars[i] = Resources.Load(namePrefix + j, Texture);

        }

        else if (j < 1000){

            bars[i] = Resources.Load(namePrefix +  j, Texture);

        }

        else{

            bars[i] = Resources.Load(namePrefix + j, Texture);

        }

    }

    return bars;

}



function OnGUI(){

    GUI.Box(Rect ((Screen.width/2)-307.75,(Screen.height/2)-250,500, 615.5),pkd,GUIStyle.none); 
    var num1 = 0;
    num=GUI.TextField(Rect ((Screen.width/2)-250, (Screen.height/2)+250, 380, 20), num, 3);
    if (GUI.Button(Rect ((Screen.width/2)-250, (Screen.height/2)+275, 380, 30),"Search Pokedex")){
            // Insert the data
            databaseData = db.BasicQuery("SELECT * FROM pokemon_species_names WHERE pokemon_species_id="+num,true);
            num1 = parseInt(num)-1;
            GUI.depth = 0;
            cube.active = true;
            cube.audio.Play();
            //GUI.DrawTexture(Rect((Screen.width/2),(Screen.height/2),96,96),imagess[num1]);
           guii.texture = imagess[num1];
           //GUI.depth = 0;
            //GUI.Box(Rect((Screen.width/2)-307.75,(Screen.height/2)-250,342,320),images[num1],GUIStyle.none);
            // And update the readout of the database
           
        }
    
    // This first block allows us to enter new entries into our table
       
            
            
        
 
        

        // This second block gives us a button that will display/refresh the contents of our database
        
 
        //GUI.Label(Rect ((Screen.width/2)-250, (Screen.height/2)+165, 380, 30)(style,"Pokedex Contents"));
        //scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Height(Screen.height-500));
        GUILayout.BeginArea(Rect ((Screen.width/2)-190, (Screen.height/2)+170, 380, 30));
            for (var line : ArrayList in databaseData){
                GUILayout.BeginHorizontal();
                for (var s in line){
                    GUILayout.Label(s.ToString(), sty,GUILayout.Width(DatabaseEntryStringWidth));
                }
                GUILayout.EndHorizontal();
            }
 
       // GUILayout.EndScrollView();
        
 GUILayout.EndArea();
    
}
 
// Wrapper function for inserting our specific entries into our specific database and table for this file

 
// Wrapper function, so we only mess with our table.
function ReadFullTable(){
    return db.ReadFullTable(TableName);
}
//function BasicQuery(p: String, n: boolean){
  //  return db.BasicQuery(p,n);
//}
 
// Another wrapper function...
function DeleteTableContents(){
    db.DeleteTableContents(TableName);
}